const chai = require('chai');
const expect = chai.expect;
const math = require('./math');

describe('testchai', ()=>{
    it('should compare thing by expect', ()=>{
        expect(1).to.equal(1);
    });
    it('should compare another things by expect', ()=>{
        expect(5>8).to.be.false;
        expect({name: 'som'}).to.deep.equal({name: 'som'});
        expect({name: 'som'}).to.have.property('name').to.equal('som');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('som').to.be.a('String');
        expect('som'.length).to.equal(3);
        expect('som').to.lengthOf(3);
    });

});

describe('Math module',()=>{
    context('Function add1',()=>{
        it('ควรส่งค่ากลับเป็นตัวเลข',()=>{
            expect(math.add1(0,0)).to.be.a('number');

        })
        it('math.add1(1,1) ควรส่งค่ากลับเป็น 2',()=>{
            expect(math.add1(1,1)).to.equal(2);

        });

    });
});